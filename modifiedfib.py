
# recursive function for modified fibonacci sequence
# wherein you define the (n) number of previous numbers to be added to determine the next in the sequence
# with 0th to (n-1)th are all equal to 0
# with nth number equal to 1
# with 3 sequence is 0 0 1 1 2 4 7 13 24 44 81 149 274 504
# with 4 sequence is 0 0 0 1 1 2 4 8 15 29 56 108 208 401
# with 5 sequence is 0 0 0 0 1 1 2 4 8 16 31 61 120 236 
# with 6 sequence is 0 0 0 0 0 1 1 2 4 8 16 32 63 125 
# with 7 sequence is 0 0 0 0 0 0 1 1 2 4 8 16 32 64

def recfib(n, m):
    if n <= m:
        if n < (m - 1): #change this to n < 1 if you want to 0th number = 0 then 1 for 1st to (n-1)th number
            return 0
        return 1
    else:
        nsum = 0
        for d in range(1, m+1, 1):
           nsum += recfib(n-d, m)
        return nsum

def fib(n, m):
    if m < 2:
        print("m should be greater than 1")
        return
    for i in range(n):
        print (recfib(i, m))


if __name__ == '__main__':

    sequence_to_add = 0
    number_sequence = 0
    noInput = True

    for j in range(2, 8, 1):
        for i in range(0, 15, 1):
            print("Fibonnaci Sequence - adding N-{0} previous, Length {1} : ".format(j, i))
            fib(i, j)

    while(noInput):
        raw = input("Plese enter a positive integer (Nth sequence to display): ")
        try:
            number_sequence = int(raw)
        except:
            print("Received a non-numeric input. ")
            pass
        if number_sequence <= 0:
            print("Received zero or negative number as input. ")
        if number_sequence > 0:
            noInput = False

    noInput = True

    while(noInput):
        raw = input("Plese enter a positive integer greater than 1 (Previous n-numbers to add for next number): ")
        try:
            sequence_to_add = int(raw)
        except:
            print("Received a non-numeric input. ")
            pass
        if sequence_to_add <= 1:
            print("Received one or lower number as input. ")
        if number_sequence > 0:
            noInput = False

    print("Fibonnaci Sequence: ")
    fib(number_sequence, sequence_to_add)
