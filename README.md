Standard Fibonacci sequence 
Reference: https://simple.wikipedia.org/wiki/Fibonacci_number

Seqeunce is as follows: 0 1 1 2 3 5 8 13 21 34 55 89 144 233

Our modified Fibonacci sequence:
- wherein you define the (n) number of previous numbers to be added to determine the next in the sequence
- with 0th to (n-1)th numbers are all equal to 0 and nth number equal to 1

```
* if n=3: sequence will be 0 0 1 1 2 4 7 13 24 44 81 149 274 504
* if n=4: sequence will be 0 0 0 1 1 2 4 8 15 29 56 108 208 401
* if n=5: sequence will be 0 0 0 0 1 1 2 4 8 16 31 61 120 236 
* if n=6: sequence will be 0 0 0 0 0 1 1 2 4 8 16 32 63 125 
* if n=7: sequence will be 0 0 0 0 0 0 1 1 2 4 8 16 32 64
```
