
# recursive function for standard fibonacci sequence
# which is 0 1 1 2 3 5 8 13 21 34 55 89 144 233
def recfib(n):
    if n <= 1:
        return n
    else:
        return (recfib(n-1) + recfib(n-2))

def fib(n):
    for i in range(n):
        print (recfib(i))


if __name__ == '__main__':

    number_sequence = 0
    noInput = True

    while(noInput):
        raw = input("Plese enter a positive integer: ")
        try:
            number_sequence = int(raw)
        except:
            print("Received a non-numeric input. ")
            pass
        if number_sequence <= 0:
            print("Received zero or negative number as input. ")
        if number_sequence > 0:
            noInput = False

    print("Fibonnaci Sequence: ")
    fib(number_sequence)
        
